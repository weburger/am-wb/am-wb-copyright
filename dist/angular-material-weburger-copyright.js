/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerCopyright', [ 'ngMaterialWeburger' ])
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 * 
 */
.run(function($widget) {
    $widget
    .newWidget({
	type : 'Copyright',
	templateUrl : 'views/angular-material-wb-copyright.html',
	label : 'Copyright',
	description : 'Simple copyright widget',
	image : 'images/wb/copyright.svg',
	link : 'https://gitlab.com/weburger/angular-material-weburger-copyright',
	setting : [ 'text', 'selfLayout', 'border',
	    'background', 'marginPadding',
	    'minMaxSize' ],
	data : {
	    type : 'Copyright',
	    text : 'copyright'
	}
    });
});

angular.module('ngMaterialWeburgerCopyright').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/angular-material-wb-copyright.html',
    "<div wb-margin=wbModel.style wb-padding=wbModel.style wb-size=wbModel.style wb-background=wbModel.style layout-align=\"start center\"> <div ng-style=\"{'width': '80%', 'margin-top': '3em', 'border-bottom': '1px solid #fff' }\"> </div> <div layout=column layout-align=\"start center\" ng-class=\"{'mde-rtl':mdeModel.style.rtl}\"> <h3>{{wbModel.title}}</h3> <div ng-bind-html=wbModel.text|wbunsafe></div> </div> </div>"
  );

}]);
