'use strict';

describe('Directive: mdeBrandAction', function () {

  // load the directive's module
  beforeEach(module('ngMaterialWeburger'));

  var scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function (/*$compile*/) {
//    element = angular.element('<mde-brand-action></mde-brand-action>');
//    element = $compile(element)(scope);
//    expect(element.text()).toBe('this is the mdeBrandAction directive');
	  expect(angular.isDefined(scope)).toBe(true);
  }));
});
